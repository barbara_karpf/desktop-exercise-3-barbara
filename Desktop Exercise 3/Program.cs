﻿using Desktop_Exercise_3.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace Desktop_Exercise_3
{
  class Program
  {
    static void Main(string[] args)
    {

      // declare a list of customers to put various query results into
      var myCustomers = new List<Customer>();

      // *************************************************************************** //
      // QUERY 1
      // *************************************************************************** //

      using (var db = new AdventureWorks2014Entities())
      {
        myCustomers = db.Customers
          .OrderBy(x => x.AccountNumber)
          .ToList();
      }
      var q1path = @"../../Query1.txt";
      using (var swq1 = new StreamWriter(q1path))
      {
        swq1.WriteLine("* CUSTOMER ACCOUNT NUMBERS *");
        myCustomers.ForEach(x => swq1.WriteLine("{0}", x.AccountNumber));
        swq1.WriteLine("THE TOTAL CUSTOMER COUNT: {0}", myCustomers.Count);
      }

      // *************************************************************************** //
      // QUERY 2 using Linq EAGER loading
      // *************************************************************************** //

      using (var db = new AdventureWorks2014Entities())
      {
        // c stands for the customers that we are selecting from our Customer table
        myCustomers = (from c in db.Customers
                       // join two tables together; customer and contact person; results in only those customers with conact person
                       join p in db.People on c.PersonID equals p.BusinessEntityID into innerjoinCustwithPerson
                       // left outer join includes customers with no contact person
                       from subp in innerjoinCustwithPerson.DefaultIfEmpty()
                       // join two tables together; customer and store info; results in only those customers with store info
                       join s in db.Stores on c.StoreID equals s.BusinessEntityID into innerjoinCustwithStore
                       // left outer join to include customers without a store defined
                       from subs in innerjoinCustwithStore.DefaultIfEmpty()
                       // join two tables together; customer and salesorderheaders; results in only customers with salesorderheaders
                       join soa in db.SalesOrderHeaders on c.CustomerID equals soa.CustomerID into innerjoinCustwithSalesOrderHeader
                       // left outer join to include customers without salesorderheaders
                       from subsoa in innerjoinCustwithSalesOrderHeader.DefaultIfEmpty()
                       // select all of these customers
                       select c)
          .Include(x => x.Person)
          .Include(x => x.Store)
          .Include(x => x.SalesOrderHeaders)
          // must add distinct to elimitate duplicate rows that get pulled with the joins
          .Distinct()
          .OrderBy(x => x.Person.LastName)
          .ThenBy(x => x.Person.FirstName)
          .ThenBy(x => x.CustomerID)
          .ToList();
      }

      OutputCustomerInfo(@"../../Query2Linq.txt", myCustomers);

      // *************************************************************************** //
      // QUERY 2 using Lamda EAGER loading
      // *************************************************************************** //

      using (var db = new AdventureWorks2014Entities())
      {
        myCustomers = db.Customers
          .Include(x => x.Person)
          .Include(x => x.Store)
          .Include(x => x.SalesOrderHeaders)
          .OrderBy(x => x.Person.LastName)
          .ThenBy(x => x.Person.FirstName)
          .ThenBy(x => x.CustomerID)
          //.Take(100)
          .ToList();
      }

      OutputCustomerInfo(@"../../Query2Lamda.txt", myCustomers);

      // *************************************************************************** //
      // QUERY 3 using EAGER loading
      // *************************************************************************** //


      using (var db = new AdventureWorks2014Entities())
      {
        myCustomers = db.Customers
          .Include(x => x.Person)
          .Include(x => x.Store)
          .Include(x => x.SalesOrderHeaders)
          .Where(x => x.Person.LastName == "Chapman")
          .OrderBy(x => x.Person.FirstName)
          .ThenBy(x => x.CustomerID)
          .ToList();
      }

      OutputCustomerInfo(@"../../Query3.txt", myCustomers);

      // *************************************************************************** //
      // QUERY 4 using EAGER loading
      // *************************************************************************** //

      using (var db = new AdventureWorks2014Entities())
      {
        myCustomers = db.Customers
          .Include(x => x.Person)
          .Include(x => x.Store)
          .Include(x => x.SalesOrderHeaders)
          .Where(x => x.SalesOrderHeaders.Sum(order => order.TotalDue) > 300000)
          .OrderBy(x => x.Person.LastName)
          .ThenBy(x => x.Person.FirstName)
          .ThenBy(x => x.CustomerID)
          .ToList();
      }

      OutputCustomerInfo(@"../../Query4.txt", myCustomers);

      // *************************************************************************** //
      // QUERY 5 using EAGER loading
      // *************************************************************************** //

      using (var db = new AdventureWorks2014Entities())
      {
        myCustomers = db.Customers
          .Include(x => x.Person)
          .Include(x => x.Store)
          .Include(x => x.SalesOrderHeaders)
          .OrderBy(x => x.Person.LastName)
          .ThenBy(x => x.Person.FirstName)
          .ThenBy(x => x.CustomerID)
          .ToList();
      }

      OutputCustomerInfo(@"../../Query5.txt", myCustomers);

      // *************************************************************************** //
      // QUERY 6 using EAGER loading
      // *************************************************************************** //

      decimal maxFreightPerc = (decimal)0.0;

      using (var db = new AdventureWorks2014Entities())
      {
        maxFreightPerc = db.SalesOrderHeaders.Max(order => order.Freight / order.TotalDue);

        myCustomers = db.Customers
          .Include(x => x.Person)
          .Include(x => x.Store)
          .Include(x => x.SalesOrderHeaders)
          .Where(x => x.SalesOrderHeaders.Max(order => order.Freight / order.TotalDue) == maxFreightPerc)
          .ToList();
      }

      OutputCustomerInfo(@"../../Query6.txt", myCustomers);

    }

    // Method for the program class
    static void OutputCustomerInfo(string path, List<Customer> myCustomerList)
    {
      using (var swq = new StreamWriter(path))
      {
        // Put at both top and bottom of file so that easy to check
        swq.WriteLine("THE TOTAL CUSTOMER COUNT: {0}\r\n", myCustomerList.Count);

        foreach (var c in myCustomerList)
        {
          swq.Write("CUSTOMER ID: {0}   ", c.CustomerID);
          swq.Write("ACCOUNT#: {0} \r\n", c.AccountNumber);
          // Not sure why StringIsNullorEmpty doesn't work
          swq.Write("NAME: {0}, {1}   ",
            (c.Person != null) ? c.Person.LastName : "No Last Name ",
            (c.Person != null) ? c.Person.FirstName : "No First Name ");

          swq.Write("STORE NAME: {0} \r\n", (c.Store != null) ? c.Store.Name : "No Store Name ");

          var salesTotal = (decimal)0;
          foreach (var order in c.SalesOrderHeaders)
          {
            swq.WriteLine("\t\t\tTOTAL DUE: {0:C} (Subtotal: {1:C}, Tax: {2:C}, Freight: {3:C} ({4:P2}))",
                          order.TotalDue, order.SubTotal, order.TaxAmt, order.Freight, (order.Freight / order.TotalDue));
            salesTotal += order.TotalDue;
          }
          swq.WriteLine("TOTAL OF ALL ORDERS: {0:C}", salesTotal);
          swq.WriteLine();
        }
        swq.WriteLine("THE TOTAL CUSTOMER COUNT: {0}\r\n", myCustomerList.Count);
      }
    }

  }  // end program class
} // end namespace











// MISC NOTES COPIED FROM PROJECTOR
//inside this using statement, db is your database connection; the source of all of your queries
//outside of the using statement, the database connection no longer exists and is cleaned up
//by the garbage collector
//using (var db = new AdventureWorks2014Entities())
//{
//// all of the people in the database
//var x = db.People.First().EmailAddresses.ToList();
//var oneEmail = x.First().EmailAddress1;

//// lazy loading list of people in the db whose first name is Dave
//var y = db.People.Where(z => z.FirstName.Equals("Dave"));
//// same statement, but without a =>; so y == peeps
//var peeps = (from p in db.People where p.FirstName.Equals("Dave") select p);

//// this is when query is actually loaded
//var emails = y.First().EmailAddresses;
// See Jesse's version for more samples



// must include include statements or else lazy loading won't allow extra tables be available
//myPeople = db.People
//  .Include(e => e.EmailAddresses)
//  .Include(c => c.Customers) 
//  .ToList();
// can also be done with a join ???????
// this is same thing as written above
//myPeople = (from p in db.People
//            join e in db.EmailAddresses on p.BusinessEntityID equals e.BusinessEntityID
//            join c in db.Customers on p.BusinessEntityID equals c.CustomerID
//            select p).ToList();


//}
// db no longer exists outside of the using block; garbage cleaner will clean it up
//int[] numbers = {6. 4. 1, 3. 9};

//var lowNums = 
//  from n in numbers where numbers < 6
//    select n;

//var lowNums2 = numbers.Where (x=> x<6);

// lazily loaded query for data
//myPeople = db.People.Take(600).ToList();
// works
//var peopleQuery = db.People.Take(600).OrderBy(x => x.LastName).ThenBy(x => x.FirstName);
//myPeople = peopleQuery.ToList();



//myCustomers = db.Customers
//   .Include( x => x.Person)
//   .Include( x => x>Person.EmailAddresses)

//   .Take(600)
//   .OrderBy(x=>x.Person.LastName)
//   .OrderBy()

// Can do .Where(x =>x.Person != null...

//path to the output file contained in the project
// relative pathing; note must go up two levels as running inside of bin
//var path = @"../../output.txt";
